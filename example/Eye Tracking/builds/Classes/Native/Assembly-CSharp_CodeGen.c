﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void EyeTracker::Awake()
extern void EyeTracker_Awake_m54A20B34D9CFAB9A2A03ECC58A76A73420542898 ();
// 0x00000002 System.Void EyeTracker::OnEnable()
extern void EyeTracker_OnEnable_mDB475A76F5D71784A3EF31E933AC0B09D3BA4205 ();
// 0x00000003 System.Void EyeTracker::OnDisable()
extern void EyeTracker_OnDisable_m66C55BDEB8F7522F564CAAAF5C35EC91A47A44B7 ();
// 0x00000004 System.Void EyeTracker::OnUpdated(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void EyeTracker_OnUpdated_mF3BD6A7F4A1F0268CA6B4551FC604AA0F4596B64 ();
// 0x00000005 System.Void EyeTracker::SetVisibility(System.Boolean)
extern void EyeTracker_SetVisibility_m63966D6BFE77487A6FFB32C6FDC0D8149C28BE7C ();
// 0x00000006 System.Void EyeTracker::Update()
extern void EyeTracker_Update_mAF8DAB9A48E944F5573101F1F49B3F89A940A682 ();
// 0x00000007 System.Void EyeTracker::.ctor()
extern void EyeTracker__ctor_mADB9C25EF0F9FC27B15B549661138F1A8BA75200 ();
static Il2CppMethodPointer s_methodPointers[7] = 
{
	EyeTracker_Awake_m54A20B34D9CFAB9A2A03ECC58A76A73420542898,
	EyeTracker_OnEnable_mDB475A76F5D71784A3EF31E933AC0B09D3BA4205,
	EyeTracker_OnDisable_m66C55BDEB8F7522F564CAAAF5C35EC91A47A44B7,
	EyeTracker_OnUpdated_mF3BD6A7F4A1F0268CA6B4551FC604AA0F4596B64,
	EyeTracker_SetVisibility_m63966D6BFE77487A6FFB32C6FDC0D8149C28BE7C,
	EyeTracker_Update_mAF8DAB9A48E944F5573101F1F49B3F89A940A682,
	EyeTracker__ctor_mADB9C25EF0F9FC27B15B549661138F1A8BA75200,
};
static const int32_t s_InvokerIndices[7] = 
{
	23,
	23,
	23,
	1514,
	31,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	7,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
